////////////////////////////////////////////////////////////////////////////////
// FILE:
//   main.rs
//
// DESCRIPTION:
//   A music player which operates on a tag + query system.
//
// AUTHOR:
//   Manu Kumar, mrkumar9001 at gmail dot com
//
// COPYRIGHT/LICENSE:
//   Copyright (C) 2020, Manu Kumar
//
//   This file is part of Ongaku.
//
//   Ongaku is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   Ongaku is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with Ongaku.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

//use log::error;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::process;
use std::string::String;

struct Library {
    path: String,
}

macro_rules! fatal {
    ($($arg:tt)*) => {{
        eprintln!("FATAL: {}", format!($($arg)*));
        process::exit(1);
    }}
}

fn main() {
    let library = Library {
        path: String::from("playlist"),
    };

    println!("attempting to open {}", library.path);

    let path = Path::new(&library.path);
    let display = path.display();

    let file = match File::open(&path) {
        Err(why) => fatal!("couldn't open {}: {}", display, why),
        Ok(file) => file,
    };

    // Read the file contents line by line.
    let reader = BufReader::new(file);

    for line in reader.lines() {
        match line {
            Err(val) => println!("ERROR: failed to read due to {}", val),
            Ok(val) => println!(" * {}", val),
        }
    }

    process::exit(0);
}
